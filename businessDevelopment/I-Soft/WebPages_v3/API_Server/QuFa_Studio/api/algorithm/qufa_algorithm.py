import pandas as pd
import numpy as np
import math
import random

from sklearn.preprocessing import LabelEncoder
from sklearn.naive_bayes import GaussianNB

import itertools
from urllib.request import Request, urlopen
from urllib.parse import urlencode, quote_plus
from pandas.io.json import json_normalize
import json

########## 공통 ##########

def load_dataset(base_url, base_key, additional_url=None, additional_key=None) : 
    
    base_queryParams = '?' + urlencode({quote_plus('key') : base_key})

    response = urlopen(base_url + base_queryParams)
    json_api = response.read().decode("utf-8")

    json_file = json.loads(json_api)
    data = pd.DataFrame(json_file['data'][1:], columns = json_file['data'][0])

    if additional_url is not None : 
        additional_queryParams = '?' + urlencode({quote_plus('key') : additional_key})

        response = urlopen(additional_url + additional_queryParams)
        json_api = response.read().decode("utf-8")

        json_file = json.loads(json_api)
        sample_data = pd.DataFrame(json_file['data'][1:], columns = json_file['data'][0])

        data = pd.concat([data, sample_data])
        data.reset_index(inplace=True, drop=True)

    for i in data.columns : 
        data[i] = data[i].astype('int')

    test = data.sample(frac=0.3, random_state=6)
    train = data.drop(test.index)
    
    return train, test


def static_fair(static_data, test_data, target, subgroup, indicator):
    cols = list(static_data.columns)
    except_target = cols.copy()
    except_target.remove(target)

    _, _, before_info_a, before_info_b = get_info(static_data, test_data, target, subgroup, except_target)

    fair_data, avg_fair = get_first_data(static_data, target, before_info_a[0], before_info_b[0], before_info_a[1], before_info_b[1])
    for i in except_target : 
        avg_fair, fair_data = make_fair(avg_fair, fair_data, i)

    fair=pd.DataFrame()
    for i,j in zip(fair_data,avg_fair) :
        fair = pd.concat([i.sample(int(j)), fair])

        
    before_result, after_result, score = verification(static_data, fair, test_data, target, subgroup, indicator)
        
    
    return fair, before_result, after_result, score
        
        
def verification(before_data, after_data, test_data, target, subgroup, indicator):
    
    cols = list(before_data.columns)
    except_target = cols.copy()
    except_target.remove(target)
    
    before_matrix_a, before_matrix_b, before_info_a, before_info_b = get_info(before_data, test_data, target, subgroup, except_target)
    after_matrix_a, after_matrix_b, after_info_a, after_info_b = get_info(after_data, test_data, target, subgroup, except_target)
    
    eqop = (abs(before_info_a[0]-before_info_b[0])-abs(after_info_a[0]-after_info_b[0]))/abs(before_info_a[0]-before_info_b[0])
    # print("Equality of Opportunity : " + str(score_TPR*100) + " %")

    eqod = ((abs(before_info_a[0]-before_info_b[0])-abs(after_info_a[0]-after_info_b[0]))/abs(before_info_a[0]-before_info_b[0]) + 
                    (abs(before_info_a[1]-before_info_b[1])-abs(after_info_a[1]-after_info_b[1]))/abs(before_info_a[1]-before_info_b[1]))/2
    # print("Equalized odds : " + str(score_TPRFPR*100) + " %")

    depa = (abs(before_info_a[3]-before_info_b[3])-abs(after_info_a[3]-after_info_b[3]))/abs(before_info_a[3]-before_info_b[3])
    # print("Demographic Parity : " + str(score_DP*100) + " %")

    treq = (abs(before_info_a[4]-before_info_b[4])-abs(after_info_a[4]-after_info_b[4]))/abs(before_info_a[4]-before_info_b[4])
    # print("Treatment Equal : " + str(score_TR*100) + " %")
    
    score = [eqop, eqod, depa, treq]
    
    
    if indicator == 'eqop' : 
        eqop_before_info_a = [before_info_a[0]]
        eqop_before_info_b = [before_info_b[0]]
        eqop_after_info_a = [after_info_a[0]]
        eqop_after_info_b = [after_info_b[0]]
        
        
        eqop_index = ['tp_a', 'tp_b', 'tn_a', 'tn_b', 'fp_a', 'fp_b', 'fn_a', 'fn_b', 'tpr_a', 'tpr_b']
        
        before_result = pd.DataFrame(before_matrix_a+before_matrix_b+eqop_before_info_a+eqop_before_info_b)
        before_result.index = eqop_index
        before_result.columns = ['eqop_before']
        
        after_result = pd.DataFrame(after_matrix_a+after_matrix_b+eqop_after_info_a+eqop_after_info_b)
        after_result.index = eqop_index
        after_result.columns = ['eqop_after']
        
        print("Equality of Opportunity : " + str(eqod*100) + " %")
        
        return before_result, after_result, eqop
        
    elif indicator == 'eqod' : 
        eqod_before_info_a = [before_info_a[0], before_info_a[1]]
        eqod_before_info_b = [before_info_b[0], before_info_b[1]]
        eqod_after_info_a = [after_info_a[0], after_info_a[1]]
        eqod_after_info_b = [after_info_b[0], after_info_b[1]]
        
        
        eqod_index = ['tp_a', 'tp_b', 'tn_a', 'tn_b', 'fp_a', 'fp_b', 'fn_a', 'fn_b', 'tpr_a', 'tpr_b', 'fpr_a', 'fpr_b']
        
        before_result = pd.DataFrame(before_matrix_a+before_matrix_b+eqod_before_info_a+eqod_before_info_b)
        before_result.index = eqod_index
        before_result.columns = ['eqod_before']
        
        after_result = pd.DataFrame(after_matrix_a+after_matrix_b+eqod_after_info_a+eqod_after_info_b)
        after_result.index = eqod_index
        after_result.columns = ['eqod_after']
        
        print("Equalized odds : " + str(eqod*100) + " %")
        
        return before_result, after_result, eqod
    
        
    elif indicator == 'depa' : 
        depa_before_info_a = [before_info_a[3]]
        depa_before_info_b = [before_info_b[3]]
        depa_after_info_a = [after_info_a[3]]
        depa_after_info_b = [after_info_b[3]]
        
        
        depa_index = ['tp_a', 'tp_b', 'tn_a', 'tn_b', 'fp_a', 'fp_b', 'fn_a', 'fn_b', 'de_a', 'de_b']
        
        before_result = pd.DataFrame(before_matrix_a+before_matrix_b+depa_before_info_a+depa_before_info_b)
        before_result.index = depa_index
        before_result.columns = ['depa_before']
        
        after_result = pd.DataFrame(after_matrix_a+after_matrix_b+depa_after_info_a+depa_after_info_b)
        after_result.index = depa_index
        after_result.columns = ['depa_after']
        
        print("Demographic Parity : " + str(depa*100) + " %")
        
        return before_result, after_result, depa
        
        
    elif indicator == 'treq' : 
        treq_before_info_a = [before_info_a[4]]
        treq_before_info_b = [before_info_b[4]]
        treq_after_info_a = [after_info_a[4]]
        treq_after_info_b = [after_info_b[4]]
        
        
        treq_index = ['tp_a', 'tp_b', 'tn_a', 'tn_b', 'fp_a', 'fp_b', 'fn_a', 'fn_b', 'tr_a', 'tr_b']
        
        before_result = pd.DataFrame(before_matrix_a+before_matrix_b+treq_before_info_a+treq_before_info_b)
        before_result.index = treq_index
        before_result.columns = ['treq_before']
        
        after_result = pd.DataFrame(after_matrix_a+after_matrix_b+treq_after_info_a+treq_after_info_b)
        after_result.index = treq_index
        after_result.columns = ['treq_after']
        
        print("Treatment Equal : " + str(treq*100) + " %")
        
        return before_result, after_result, treq
    
    
    else : 
        
        all_index = ['tp_a', 'tp_b', 'tn_a', 'tn_b', 'fp_a', 'fp_b', 'fn_a', 'fn_b', 
                     'tpr_a', 'tpr_b', 'fpr_a', 'fpr_b', 'fnr_a', 'fnr_b', 'de_a', 'de_b', 'tr_a', 'tr_b']
        
        
        before_result = pd.DataFrame(before_matrix_a+before_matrix_b+before_info_a+before_info_b)
        before_result.index = all_index
        before_result.columns = ['all_before']
        
        after_result = pd.DataFrame(after_matrix_a+after_matrix_b+after_info_a+after_info_a)
        after_result.index = all_index
        after_result.columns = ['all_before']
        
        
        score_index = ['eqop', 'eqod', 'depa', 'treq']
        score_result = pd.DataFrame(score)
        score_result.index = score_index
        score_result.columns = ['score']
        
        
        print("Equality of Opportunity : " + str(score[0]*100) + " %")
        print("Equalized odds : " + str(score[1]*100) + " %")
        print("Demographic Parity : " + str(score[2]*100) + " %")
        print("Treatment Equal : " + str(score[3]*100) + " %")
        
        avg_score = sum(score)/4
        print("Avg : "+str(avg_score*100) + " %")
        

        return before_result, after_result, score_result



def get_matrix(y_test, y_hat) : 
    tp = np.sum((y_test ==1) & (y_hat==1) )
    tn = np.sum((y_test ==0) & (y_hat==0) )
    fp = np.sum((y_test ==0) & (y_hat==1) )
    fn = np.sum((y_test ==1) & (y_hat==0) )
    
    #accuracy = np.mean(np.equal(y_test,y_hat))
    
    return tp, tn, fp, fn


def get_info(train_data, test_data, target, subgroup, except_target) : 
    y_train=train_data[target].astype(int)
    X_train=train_data[except_target].astype(int)

    CATEGORY  =  subgroup
    SUBGROUP = 0 
    X_test_a  = test_data.loc[test_data[CATEGORY] == SUBGROUP][except_target]
    y_test_a  = test_data.loc[test_data[CATEGORY] == SUBGROUP][target]

    SUBGROUP = 1 
    X_test_b  = test_data.loc[test_data[CATEGORY] == SUBGROUP][except_target]
    y_test_b  = test_data.loc[test_data[CATEGORY] == SUBGROUP][target]

    model = GaussianNB()
    model.fit(X_train, y_train)

    y_hat = model.predict(X_test_a)
    before_tp_a, before_tn_a, before_fp_a, before_fn_a = get_matrix(y_test_a, y_hat)

    before_tpr_a = before_tp_a/(before_tp_a+before_fn_a)
    before_fpr_a = before_fp_a/(before_fp_a+before_tn_a)
    before_fnr_a = before_fn_a/(before_tp_a+before_fn_a)

    before_dp_a = (before_tp_a+before_tn_a)/(before_tp_a+before_tn_a+before_fp_a+before_fn_a)
    before_te_a = before_fpr_a / before_fnr_a
    
    
    y_hat = model.predict(X_test_b)
    before_tp_b, before_tn_b, before_fp_b, before_fn_b = get_matrix(y_test_b, y_hat)

    before_tpr_b = before_tp_b/(before_tp_b+before_fn_b)
    before_fpr_b = before_fp_b/(before_fp_b+before_tn_b)
    before_fnr_b = before_fn_b/(before_tp_b+before_fn_b)

    before_dp_b = (before_tp_b+before_tn_b)/(before_tp_b+before_tn_b+before_fp_b+before_fn_b)
    before_te_b = before_fpr_b / before_fnr_b
    
    
    matrix_a = [ before_tp_a, before_tn_a, before_fp_a, before_fn_a ]
    matrix_b = [ before_tp_b, before_tn_b, before_fp_b, before_fn_b ]
    
    info_a = [before_tpr_a, before_fpr_a, before_fnr_a, before_dp_a, before_te_a]
    info_b = [before_tpr_b, before_fpr_b, before_fnr_b, before_dp_b, before_te_b]
    
    return matrix_a, matrix_b, info_a, info_b


# def print_score(before_info_a, before_info_b, after_info_a, after_info_b) : 
#     eqod = (abs(before_info_a[0]-before_info_b[0])-abs(after_info_a[0]-after_info_b[0]))/abs(before_info_a[0]-before_info_b[0])
#     # print("Equality of Opportunity : " + str(score_TPR*100) + " %")

#     score_TPRFPR = ((abs(before_info_a[0]-before_info_b[0])-abs(after_info_a[0]-after_info_b[0]))/abs(before_info_a[0]-before_info_b[0]) + 
#                     (abs(before_info_a[1]-before_info_b[1])-abs(after_info_a[1]-after_info_b[1]))/abs(before_info_a[1]-before_info_b[1]))/2
#     # print("Equalized odds : " + str(score_TPRFPR*100) + " %")

#     score_ED = (abs(before_info_a[3]-before_info_b[3])-abs(after_info_a[3]-after_info_b[3]))/abs(before_info_a[3]-before_info_b[3])
#     # print("Demographic Parity : " + str(score_DP*100) + " %")

#     score_TE = (abs(before_info_a[4]-before_info_b[4])-abs(after_info_a[4]-after_info_b[4]))/abs(before_info_a[4]-before_info_b[4])
#     # print("Treatment Equal : " + str(score_TR*100) + " %")

# #     avg_score = (score_TPR+score_TPRFPR+score_ED+score_TE)/4
# # #     print(
# # #           " Avg : "+str(avg_score*100)[:5] + " %" 
# # #         + " Equal Opportunity : "+str(score_TPR*100)[:5] + " %" 
# # #         + " Equalized Odd : "+str(score_TPRFPR*100)[:5] + " %" 
# # #         + " Equalizing Disincentives : "+str(score_ED*100)[:5] + " %" 
# # #         + " Treatment Equal : "+str(score_TE*100)[:5] + " %"
# # #          )
# # #     print("--------------------------------------------------------")
    
#     return score_TPR, score_TPRFPR, score_ED, score_TE



########## 정적보상 ##########

def get_avg( arg, ratio ) : 
    arg_sum = 0
    arg_len = len(arg)
    avg_ratio=[]
    avg_fair=[]
    
    for i in range(arg_len) : 
        arg_sum+=(len(arg[i])*ratio)
    arg_avg = arg_sum/arg_len
        
    for i in range(arg_len) : 
        if len(arg[i])>arg_avg : 
            avg_fair.append(arg_avg)
        else : avg_fair.append(len(arg[i]))
            
    return arg_avg, avg_fair

def get_first_data( df, cur_col, tpra, tprb , fpra, fprb ) : 
    uniq = df[cur_col].unique()
    return_li = []
    for i in uniq : 
        return_li.append(df[df[cur_col]==i])

    arg_len = len(return_li)
    avg_ratio=abs(abs(tpra-tprb)+abs(fpra-fprb))/2
    avg_fair=[]
    arg_sum = 0
    
    for i in range(arg_len) : 
        arg_sum+=len(return_li[i])
    arg_avg = arg_sum/arg_len
    
    for i in range(arg_len) : 
        if len(return_li[i])>arg_avg : 
            avg_fair.append(len(return_li[i])-(len(return_li[i])-arg_avg)*avg_ratio)
        else : avg_fair.append(len(return_li[i]))
            
    return return_li, avg_fair

def get_next_data( df, cur_col, ratio ) : 
    uniq = df[cur_col].unique()
    return_li = []
    for i in uniq : 
        return_li.append(df[df[cur_col]==i])
    arg_avg, avg_fair= get_avg(return_li, ratio)
    return return_li, avg_fair


def make_fair(avg_fair, fair_data, col):

    tmp_fair_data=[]
    tmp_avg_li=[]

    for i,j in zip(fair_data,avg_fair) : 
        if j < len(i) : 
            ratio=1-((len(i)-j)/len(i))
        else : ratio=1
        #print(ratio)        
        return_li,avg = get_next_data(i, col, ratio)

        tmp_avg_li.append(avg)
        tmp_fair_data.append(return_li)

    tmp_fair_data=list(itertools.chain(*tmp_fair_data))
    #print(len(b))

    avg_fair=list(itertools.chain(*tmp_avg_li))
    
    return avg_fair, tmp_fair_data
