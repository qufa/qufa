#!/bin/bash

export FLASK_APP=app.py

# Debug
#flask run -h 0.0.0.0 -p 5555

# Release
nohup flask run -h 0.0.0.0 -p 5555 > app.log 2>&1 &
#nohup flask run -h 0.0.0.0 -p 5555 > /dev/null 2>&1 &
