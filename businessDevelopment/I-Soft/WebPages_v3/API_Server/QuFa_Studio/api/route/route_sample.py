from flask import Blueprint, render_template, jsonify
import datetime
import sqlalchemy as db

engine = db.create_engine("mariadb+mariadbconnector://qufa:Qufa!234@127.0.0.1:6606/qufa")

bp_sample = Blueprint('sample', __name__)


@bp_sample.route('/', methods=['GET'])
def index():
	return render_template('index.html')


@bp_sample.route('/sample1', methods=['GET'])
def sample1():
	return jsonify({'now': datetime.datetime.now()})


@bp_sample.route('/sample2', methods=['GET'])
def sample2():
	conn = engine.connect()
	query = 'select now()'
	result = conn.execute(query);
	row = result.fetchone()
	return jsonify({'now': row[0]})

